 # -*- coding: utf-8 -*-
##############################################################################
#		
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013 webkul
#	 Author :
#				www.webkul.com	
#
##############################################################################
{
    'name': 'Opencart-OpenERP Stock Management',
    'version': '2.3.5',
    'category': 'Generic Modules',
    'sequence': 1,
    'summary': 'Manage Stock with Opencart Connector',
    'description': """	
    This Module helps in maintaining stock between openerp and opencart with real time.
	
	NOTE : This module works very well with latest version of opencart 1.7 and latest version of OpenErp 7.0.
    """,
    'author': 'Webkul Software Pvt Ltd.',
    'depends': ['opencart_erp_connector'],
    'website': 'http://www.webkul.com',
    'update_xml': ['opencart_openerp_stock_view.xml'],
    'installable': True,
    'active': False,
    #'certificate': '0084849360985',
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
