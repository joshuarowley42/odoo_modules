 # -*- coding: utf-8 -*-
##############################################################################
#		
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013 webkul
#	 Author :
#				www.webkul.com	
#
##############################################################################

import sys 
import openerp.pooler
import openerp.netsvc
from openerp.osv import fields, osv
from openerp.tools.translate import _
import time
import datetime
from ..opencart_erp_connector import SOAPpy
from openerp import workflow

################## .............magento-openerp stock.............##################

class stock_move(osv.osv):
	_inherit="stock.move"
	
	def action_done(self, cr, uid, ids, context=None):
		""" Makes the move done and if all moves are done, it will finish the picking.
		@return:
		"""
		res = super(stock_move, self).action_done(cr, uid, ids, context=context)
		for id in ids:
			data=self.browse(cr,uid,id)
			flag=1
			product_pool = self.pool.get('product.product')
			if data.origin!=False:
				if data.origin.startswith('SO'):
					sale_id = self.pool.get('sale.order').search(cr,uid,[('name','=',data.origin),('opencart_id','!=',False)])
					if sale_id:
						flag=0
			else:
				flag = 2	
			if flag == 1:				
				erp_product_id = data.product_id.id
				product_qty = product_pool.browse(cr, uid, erp_product_id).qty_available
				self.synch_quantity(cr, uid, erp_product_id,product_qty)
			if flag==2:
				erp_product_id = data.product_id.id
				check_in = self.pool.get('stock.warehouse').search(cr,uid,[('lot_stock_id','=',data.location_dest_id.id),('company_id','=',data.company_id.id)],limit=1)
				if check_in:
					# Getting Goods.				
					product_qty = product_pool.browse(cr, uid, erp_product_id).qty_available
					self.synch_quantity(cr, uid, erp_product_id, product_qty)
				check_out = self.pool.get('stock.warehouse').search(cr,uid,[('lot_stock_id','=',data.location_id.id),('company_id','=',data.company_id.id)],limit=1)
				if check_out:
					# Sending Goods.
					product_qty = product_pool.browse(cr, uid, erp_product_id).qty_available
					self.synch_quantity(cr, uid, erp_product_id,product_qty)
		return True
	
	def synch_quantity(self, cr, uid, erp_product_id, product_qty, context=None):	
                return True
		response=self.update_quantity(cr,uid,erp_product_id,product_qty)
		if response[0]==1:
			return True
		
	def update_quantity(self, cr, uid, erp_product_id, quantity):
		qty = 0
		session = 0
		text = ''
		stock = 0
		product_pool = self.pool.get('product.product')
		check_mapping = product_pool.search(cr,uid,[('id','=',erp_product_id),('opencart_id','!=',False)])
		if check_mapping:
			map_obj = product_pool.browse(cr,uid,check_mapping[0])
			oc_product_id = map_obj.opencart_id
			config_id=self.pool.get('opencart.configuration').search(cr,uid,[('active','=',True)])
			if not config_id:
				return [0,' Connection needs one Active Configuration setting.']
			if len(config_id)>1:
				return [0,' Sorry, only one Active Configuration setting is allowed.']
			else:
				param={}		
				session=''
				obj = self.pool.get('opencart.configuration').browse(cr,uid,config_id[0])
				url = obj.api_url+'/api/server.php'
				param['api_user'] = obj.api_user
				param['api_key'] = obj.api_key
				try:
					server = SOAPpy.SOAPProxy(url) 
					session = server.login(param)
				except Exception,e:
					text = 'Error in Opencart Connection.'+str(e)
				if not session:
					return [0,text]
				else:
					params={}
					params['product_id'] = oc_product_id
					params['stock'] = quantity
					try:
						server.UpdateProductStock(session,params)
						return [1,'']
					except Exception,e:
						return [0,' Error in Updating Quantity for Opencart Product Id %s.'%oc_product_id]
		else:
			return [0,'Error in Updating Stock, Product Id %s not mapped.'%erp_product_id]
stock_move()
