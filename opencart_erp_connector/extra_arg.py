# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import sys 
import openerp.pooler
import openerp.netsvc
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from time import strptime
import time
from decimal import *
from xmlrpclib import *
import xml
import xmlrpclib
import openerp.tools
from openerp.exceptions import Warning
from openerp import workflow

try:
	from openerp.loglevels import ustr as pob_decode
except:
	from openerp.tools.misc import ustr as pob_decode

def _unescape(text):
	##
	# Replaces all encoded characters by urlib with plain utf8 string.
	#
	# @param text source text.
	# @return The plain text.
	from urllib import unquote_plus
	return unquote_plus(text.encode('utf8'))
	
def _decode(name):
	# name = name.translate(None,'=+')
	name = urllib2.unquote(name)
	#DB is corrupted with utf8 and latin1 chars.
	decoded_name = name
	if isinstance(name, unicode):
		try:
			decoded_name = name.encode('utf8')
		except:
			decoded_name = name
	else:
		try:
			decoded_name = unicode(name, 'utf8')
		except:
			try:
				decoded_name = unicode(name, 'latin1').encode('utf8')
			except:
				decoded_name = name
	return decoded_name

############## OpenCart classes #################
class extra_function(osv.osv):
	_name="extra.function"

	def create_product(self, cr, uid, data, context=None):
		"""Create and update a product by any webservice like xmlrpc.
		@param data: details of product fields in list.
		"""
		vals = {}		
		product_id = 0
		if data.has_key('name')  and data['name']:
			vals['name'] = data.get('name')
		if data.has_key('default_code') and data['default_code']:
			vals['default_code'] = data.get('default_code')
		if data.has_key('description') and data['description']:
			vals['description'] = data.get('description')
		if data.has_key('description_sale') and data['description_sale']:
			vals['description_sale'] = data.get('description_sale')
		vals['list_price'] = data.get('list_price') or 0.00
		vals['weight_net'] = data.get('weight_net') or 0.00
		if data.has_key('standard_price'):
			vals['standard_price'] = data.get('standard_price')
		else:
			vals['weight'] = data.get('weight_net') or 0.00
		if data.has_key('weight'):
			vals['weight'] = data.get('weight')
		else:
			vals['weight'] = data.get('weight_net') or 0.00
		if data.has_key('type'):
			vals['type'] = data.get('type')
		if data.has_key('categ_ids'):
			categ_ids = data.get('categ_ids')
			vals['categ_id'] = max(categ_ids)
			categ_ids.remove(max(categ_ids))
			vals['categ_ids'] = [(6, 0, categ_ids)]
		if data.has_key('image'):
			vals['image'] = data.get('image')
		if data.has_key('ean13'):
			vals['ean13'] = data.get('ean13')
		if data.has_key('opencart_id'):
			vals['opencart_id'] = data.get('opencart_id')
		if data.get('method') == 'create':
			product_id = self.pool.get('product.product').create(cr, uid, vals, context)
			return product_id
		if data.get('method') == 'write':
			product_id = data.get('product_id')
			self.pool.get('product.product').write(cr, uid, product_id, vals, context)
			return True
		return False

	def update_quantity(self,cr,uid,data,context=None):
		""" Changes the Product Quantity by making a Physical Inventory through any service like xmlrpc.
		@param data: Dictionary of product_id and new_quantity
		@param context: A standard dictionary
		@return: True
		"""
		if context is None:
			context = {}
		context['opencart']='opencart'
		rec_id = data.get('product_id')
		assert rec_id, _('Active ID is not set in Context')
		if int(data.get('new_quantity')) < 0:
			raise osv.except_osv(_('Warning!'), _('Quantity cannot be negative.'))
		if int(data.get('new_quantity')) == 0:
			return True
		inventry_obj = self.pool.get('stock.inventory')
		inventry_line_obj = self.pool.get('stock.inventory.line')
		prod_obj_pool = self.pool.get('product.product')
		res_original = prod_obj_pool.browse(cr, uid, rec_id, context=context)
		if int(data.get('new_quantity'))==int(res_original.qty_available):
			return True
		location_id = self.pool.get('stock.location').search(cr, uid, [('name','=','Stock')])
		
		inventory_id = inventry_obj.create(cr , uid, {'name': _('INV: %s') % openerp.tools.ustr(res_original.name)}, context=context)
		line_data ={
						'inventory_id' : inventory_id,
						'product_qty' : data.get('new_quantity'),
						'location_id' : location_id[0],
						'product_id' : rec_id,
						'product_uom_id' : res_original.uom_id.id
					}
		inventry_line_obj.create(cr , uid, line_data, context=context)
		inventry_obj.action_done(cr, uid, [inventory_id], context=context)
		return True

	def pricelist_currency(self,cr,uid,currency_name,currency_code,context=None):
		currency_id = self.pool.get('res.currency').search(cr,uid,[('name','=',currency_code),('active', '=', '1')])
		if currency_id:
			pricelist = {
						   'name': currency_name,
						   'active': '1',
						   'type': 'sale',
						   'currency_id': currency_id[0],
						}
			pricelist_id = self.pool.get("product.pricelist").create(cr, uid,pricelist)
			pricelist_version = {
						   'pricelist_id': pricelist_id,
						   'name':  'Default'+currency_name,
						   'active':True,
						   
						}
			price_version_id = self.pool.get('product.pricelist.version').create(cr,uid,pricelist_version)
			price_type_id = self.pool.get('product.price.type').search(cr,uid,[('field','=','list_price')])
			if price_type_id:
				product_pricelist_item = {
								'name':'Dafault'+currency_name,
								'price_version_id':price_version_id,
								'base': price_type_id[0],
							}
				product_pricelist_item_id=self.pool.get('product.pricelist.item').create(cr,uid,product_pricelist_item)
			return pricelist_id
		else:
			return -1

	def create_payment_method(self, cr, uid,data,context=None):
		if context is None: context = {}
		res = self.pool.get('account.journal').search(cr, uid, [('type', '=','bank')], limit=1)[0]
		credit_account_id=self.pool.get('account.journal').browse(cr,uid,res).default_credit_account_id.id
		debit_account_id=self.pool.get('account.journal').browse(cr,uid,res).default_debit_account_id.id
		journal = {
					   'name': self._get_journal_name(cr,uid,data.get('name')),
					   'code': self._get_journal_code(cr,uid,data.get('name')),
					   'type': 'cash',
					   #'company_id': 1,
					   #'user_id':1,
					   'default_credit_account_id':credit_account_id,
					   'default_debit_account_id':debit_account_id,
					  	}
		journal_id=self.pool.get('account.journal').create(cr,uid,journal)
		return journal_id
	
	def _get_journal_code(self, cr, uid,string,sep=' '):
		tl = []
		for t in string.split(sep):
			tl.append(t.title()[0])
		code=''.join(tl)
		code=code[0:3]
		is_exist=self.pool.get('account.journal').search(cr, uid, [('code', '=',code)])
		if is_exist:
			for i in range(99):
				is_exist=self.pool.get('account.journal').search(cr, uid, [('code', '=',code+str(i))])
				if not is_exist:
					return code+str(i)[-5:]
		return code
	
	def _get_journal_name(self, cr, uid,string):
		is_exist=self.pool.get('account.journal').search(cr, uid, [('name', '=',string)])
		if is_exist:
			for i in range(99):
				is_exist=self.pool.get('account.journal').search(cr, uid, [('name', '=',string+str(i))])
				if not is_exist:
					return string+str(i)
		return string

	def create_n_confirm_order(self,cr,uid,order_data,line_data,context=None):
		if context is None:
			context = {}
		return_array={}
		if order_data and line_data:
			order_dic={
				'partner_id'			:order_data['partner_id'],
				'partner_invoice_id'	:order_data['partner_invoice_id'],
				'partner_shipping_id'	:order_data['partner_shipping_id'],
				'pricelist_id'			:order_data['pricelist_id'],
				'carrier_id'			:order_data['carrier_id'],
                'order_policy'          :'manual',
                'opencart_id'			:order_data['opencart_order_id']
				# 'origin'				:'OpenCart'+'('+order_data['oc_reference']+')',
				# 'channel'				:'OpenCart',
				}
			# if order_data.has_key('invoice_date'):

				# context['invoice_date']=order_data.get('invoice_date',False)
			if order_data.has_key('date_add'):	
				order_dic['date_order']=order_data.get('date_add',False)
				# order_dic['date_confirm']=order_data.get('date_upd',False)
				order_dic['date_confirm']=order_data.get('date_add',False)
				context['invoice_date']=order_data.get('date_add',False)
			if order_data.has_key('shop_id') and order_data.get('shop_id'):
				order_dic.update({'shop_id':order_data['shop_id']})
			order_id=self.pool.get('sale.order').create(cr,uid,order_dic)
			if order_id:
				for line in line_data:
					if line.get('type').startswith('S'):
						erp_product_id=False
					if line.get('type').startswith('V'):
						erp_product_id=False
					if line.get('type').startswith('P'):
						erp_product_id=line['product_id']
					line_dic={
					'order_id'				:order_id,
					'product_id'			:erp_product_id,
					'price_unit'			:line['price_unit'],
					'product_uom_qty'		:line['product_uom_qty'],
					'product_uom'			:1,
					'name'					:_unescape(line['name']),
					'discount'				:line.get('discount',False),
					}
					if line.get('tax_id') and type(line.get('tax_id'))==list:
						line_dic['tax_id']=[(6,0,line.get('tax_id'))]
					else:
						line_dic['tax_id'] = False
					line_id=self.pool.get('sale.order.line').create(cr,uid,line_dic)
				
				order_erp=self.pool.get('sale.order').read(cr,uid,order_id,['name','amount_total'])
				order_name=order_erp['name']
				erp_total=order_erp['amount_total']
				cr.commit()
			return [{'erp_order_id':order_id,'opencart_order_id':order_data['opencart_order_id'],'erp_order_name':order_name,'erp_total':erp_total,'cart_total':order_data['cart_total']}]

	# Code for Cancelling an order from Opencart......

	def order_cancel(self, cr, uid, order_id, context=None):
		"""Cancel an order by any webservice like xmlrpc.
		@param order_id: OpenERP Order ID.
		@param context: A standard dictionary
		@return: True
		"""
		if context is None:
			context = {}		
		active_id = self.pool.get('opencart.configuration').search(cr, uid,[('active','=',True)])
		if active_id:
			self.pool.get('opencart.configuration').write(cr, uid, active_id[0], {'active': False})
		sale_order_pool = self.pool.get('sale.order')
		obj = sale_order_pool.browse(cr, uid, order_id)		
		if obj.shipped == False:
			acc_inv_pool = self.pool.get('account.invoice')
			stock_pick_pool = self.pool.get('stock.picking')
			acc_journal_pool = self.pool.get('account.journal')
			acc_voc_pool = self.pool.get('account.voucher')
			try:
				search_id = acc_inv_pool.search(cr, uid,[('origin','=',obj.name)])
				if search_id:
					invoice_obj = acc_inv_pool.browse(cr, uid, search_id)
					c_id = invoice_obj[0].journal_id.id	
					acc_journal_pool.write(cr, uid, c_id,{'update_posted':True})
				paid_id = acc_inv_pool.search(cr, uid,[('state','=','paid'),('origin','=',obj.name)])
				if paid_id:
					invoice_obj = acc_inv_pool.browse(cr, uid, paid_id)
					for i in invoice_obj[0].payment_ids:
						num = i.name
					sr_id = acc_voc_pool.search(cr, uid,[('move_ids.name','=',num)])
					inv_obj = acc_voc_pool.browse(cr, uid, sr_id)
					j_id = inv_obj[0].journal_id.id
					acc_journal_pool.write(cr, uid, j_id,{'update_posted':True})
					acc_voc_pool.cancel_voucher(cr, uid, sr_id, context=None)
				acc_inv_pool.action_cancel(cr, uid, search_id, context)
				s_id = stock_pick_pool.search(cr, uid,[('origin','=',obj.name)])
				if s_id:
					stock_pick_pool.action_cancel(cr, uid, s_id, context)
				sale_order_pool.action_cancel(cr, uid, [order_id], context)
			except Exception,e:
				pass		
		if active_id:
			self.pool.get('opencart.configuration').write(cr, uid, active_id[0], {'active': True})
		return True

	# code for shipping an order......

	def order_shipped(self, cr ,uid, order_id, context=None):
		if context is None:
			context = {}
		active_id = self.pool.get('opencart.configuration').search(cr, uid,[('active','=',True)])		
		if active_id:
			self.pool.get('opencart.configuration').write(cr, uid, active_id[0], {'active': False})
		read_order = self.pool.get('sale.order').read(cr,uid,[order_id],['state'])					
		if read_order[0]['state'] in ['draft','sent']:			
			self.pool.get('sale.order').signal_workflow(cr, uid, [order_id],'order_confirm')
		context['stock_from'] = 'opencart'
		order_name = self.pool.get('sale.order').name_get(cr, uid, order_id)		
		pick_id = self.pool.get('stock.picking').search(cr, uid,[('origin','=',order_name[0][1])])		
		if pick_id:
			self.pool.get('stock.picking').do_transfer(cr, uid, pick_id, context)
			workflow.trg_validate(uid, 'sale.order',order_id, 'ship_end', cr)
			if active_id:
				self.pool.get('opencart.configuration').write(cr, uid, active_id[0], {'active': True})
			return True
		else:
			return False

	def create_order_invoice(self, cr, uid, data, context=None):
		if context is None:
			context = {}		
		sale_obj = self.pool.get('sale.order')
		check = sale_obj.read(cr, uid, [data['order_id']],['state','invoice_ids'])				
		if check[0]['state'] in ['draft','sent']:
			self.pool.get('sale.order').signal_workflow(cr, uid, [data['order_id']],'order_confirm')
		if not check[0]['invoice_ids']:			
			inv_ids = self.pool.get('sale.order').manual_invoice(cr,uid,[data['order_id']],context)		
			invoice_id = inv_ids['res_id']			
			if data.has_key('date'):
				self.pool.get('account.invoice').write(cr,uid,invoice_id,{'date_invoice':data.get('date',False)})					
			self.pool.get('account.invoice').signal_workflow(cr, uid, [invoice_id],'invoice_open')
			workflow.trg_validate(uid, 'sale.order',data['order_id'], 'invoice_end', cr)
			return invoice_id
		return check[0]['invoice_ids']

		# code for Payment an order...... 
	def _get_journal_id(self, cr, uid, context=None):
		if context is None: context = {}
		if context.get('invoice_id', False):
			currency_id = self.pool.get('account.invoice').browse(cr, uid, context['invoice_id'], context=context).currency_id.id
			journal_id = self.pool.get('account.journal').search(cr, uid, [('currency', '=', currency_id)], limit=1)
			return journal_id and journal_id[0] or False
		res = self.pool.get('account.journal').search(cr, uid, [('type', '=','bank')], limit=1)
		return res and res[0] or False
	
	def _get_tax_id(self, cr, uid,journal_id,context=None):
		if context is None: context = {}
		journal = self.pool.get('account.journal').browse(cr, uid, journal_id, context=context)
		account_id = journal.default_credit_account_id or journal.default_debit_account_id
		if account_id and account_id.tax_ids:
			tax_id = account_id.tax_ids[0].id
			return tax_id
		return False
	
	def _get_currency_id(self, cr, uid,journal_id,context=None):
		if context is None: context = {}
		journal = self.pool.get('account.journal').browse(cr, uid, journal_id, context=context)
		if journal.currency:
			return journal.currency.id
		return self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id.currency_id.id

	def sales_order_payment(self, cr, uid, payment, context=None):	
		"""
		@param payment: List of invoice_id, reference, partner_id ,journal_id and amount
		@param context: A standard dictionary
		@return: True
		"""
		if context is None:
			context = {}
		amount = 0.0
		invoice_name = ' '
		active_id = self.pool.get('opencart.configuration').search(cr, uid,[('active','=',True)])
		if active_id:
			self.pool.get('opencart.configuration').write(cr, uid, active_id[0], {'active': False})
		invoice_id = self.create_order_invoice(cr, uid, payment, context)
		voucher_obj = self.pool.get('account.voucher')
		voucher_line_obj = self.pool.get('account.voucher.line')
		partner_id = payment.get('partner_id')
		journal_id = payment.get('journal_id',False)
		if not journal_id:
			journal_id = self._get_journal_id(cr, uid, context)				
		date = payment.get('date', time.strftime('%Y-%m-%d'))
		currency_id1 = self._get_currency_id(cr, uid, journal_id)
		data = voucher_obj.onchange_partner_id(cr, uid, [], partner_id, journal_id,int(amount), currency_id1, 'receipt', date, context)['value']
		invoice_name = self.pool.get('account.invoice').browse(cr,uid,invoice_id).number	
		for line_cr in data.get('line_cr_ids'):
			if line_cr['name'] == invoice_name:
				amount = line_cr['amount_original']
		account_id = data['account_id']
		statement_vals = {
                            'reference': invoice_name,
                            'journal_id': journal_id,
                            'amount': amount,
                            'date' : date,
                            'partner_id': partner_id,
                            'account_id': account_id,
                            'type': 'receipt',
                         }
		if data.get('payment_rate_currency_id'):
			statement_vals['payment_rate_currency_id'] = data['payment_rate_currency_id']
			company_currency_id=self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id.currency_id.id
			if company_currency_id<>data['payment_rate_currency_id']:
				statement_vals['is_multi_currency']=True
	
		if data.get('paid_amount_in_company_currency'):
			statement_vals['paid_amount_in_company_currency'] = data['paid_amount_in_company_currency']
		if data.get('writeoff_amount'):
			statement_vals['writeoff_amount'] = data['writeoff_amount']
		if data.get('pre_line'):
			statement_vals['pre_line'] = data['pre_line']
		if data.get('payment_rate'):
			statement_vals['payment_rate'] = data['payment_rate']
		statement_id = voucher_obj.create(cr, uid, statement_vals, context)
		for line_cr in data.get('line_cr_ids'):
			line_cr.update({'voucher_id':statement_id})
			if line_cr['name'] == invoice_name:
				line_cr['amount'] = line_cr['amount_original']
				line_cr['reconcile'] = True
			line_cr_id = self.pool.get('account.voucher.line').create(cr,uid,line_cr)
		for line_dr in data.get('line_dr_ids'):
			line_dr.update({'voucher_id':statement_id})
			line_dr_id = self.pool.get('account.voucher.line').create(cr,uid,line_dr)

		val = self.pool.get('account.voucher').signal_workflow(cr, uid, [statement_id],'proforma_voucher')

		if active_id:
			self.pool.get('opencart.configuration').write(cr, uid, active_id[0], {'active': True})
		return True	

extra_function()
