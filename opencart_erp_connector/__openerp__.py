# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'An OpenCart2OpenERP Connector',
    'sequence': 1,
    'summary': 'Cart2ERP Connector(C2E Connector)',
    'version': '2.0',
    'category': 'Generic Modules',
    'description': """
An OpenCart2OpenERP Connector.
===========================================================

This Brilliant Module will Connect Openerp with Opencart and synchronise all of your category, product, customer
---------------------------------------------------------------------------------------------------------------
and existing sales order(Invoice, shipping).
--------------------------------------------

Some of the brilliant feature of the module:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    1. synchronise all the catalog categories to Opencart.
    
    2. synchronise all the catalog products to Opencart.
    
    3. synchronise all the existing sales order(Invoice, shipping) to Opencart.
    
    4. Update all the store customers to Opencart.
    
    5. synchronise product inventory of catelog products.
    
This module works very well with opencart 1.5.* and latest version of Odoo v8.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
For any doubt or query email us at support@webkul.com or raise a Ticket on http://webkul.com/ticket/
    """,
    'author': 'Webkul Software Pvt. Ltd.',
    'depends': ['base','sale','product','stock','account_accountant','account_cancel','delivery'],
    'website': 'http://www.webkul.com',
    'data': [   
            'security/bridge_security.xml',
            'security/ir.model.access.csv',
            'wizard/wizard_message_view.xml',
            'wizard/status_wizard_view.xml',
            'core_updated_files_view.xml',
            'indian_states.xml',
            'opencart_connector_sequence.xml'
            ],
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
