# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import openerp.pooler
import openerp.netsvc
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
import time
from decimal import *
import openerp.tools
import datetime
import urllib2
import SOAPpy
from openerp import SUPERUSER_ID, api
try:
	from openerp.loglevels import ustr as pob_decode
except:
	from openerp.tools.misc import ustr as pob_decode

def _unescape(text):
	##
	# Replaces all encoded characters by urlib with plain utf8 string.
	#
	# @param text source text.
	# @return The plain text.
	from urllib import unquote_plus
	return unquote_plus(text.encode('utf8'))
	
def _decode(name):
	# name = name.translate(None,'=+')
	name = urllib2.unquote(name)
	#DB is corrupted with utf8 and latin1 chars.
	decoded_name = name
	if isinstance(name, unicode):
		try:
			decoded_name = name.encode('utf8')
		except:
			decoded_name = name
	else:
		try:
			decoded_name = unicode(name, 'utf8')
		except:
			try:
				decoded_name = unicode(name, 'latin1').encode('utf8')
			except:
				decoded_name = name
	return decoded_name

	

############## OpenCart Credentials class #################
class opencart_configuration(osv.osv):			
	_name="opencart.configuration"
	_columns = {
		'api_url': fields.char('Root URL',size=100,help="e.g:-'http://www.opencart.com/upload'"),
		'api_user':fields.char('API User'),
		'api_key':fields.char('API key',size=100),
		'status':fields.char('Connection Status',readonly=True, size=255),
		'order_status':fields.boolean('Allow Auto Order Status Update', help="If Enabled, then it'll Update Status at Opencart When Odoo Order status will changed."),
		'active':fields.boolean('Active'),
	}
	_defaults ={
		'active':lambda *a: 1,	

	}
	
	def test_connection(self, cr, uid, ids, context=None):
		text = 'test connection Un-successful please check the opencart api credentials!!!'
		status = 'OpenCart Connection Un-successful'
		param={}		
		session=''
		obj = self.browse(cr,uid,ids[0])
		url = obj.api_url+'/api/server.php'
		param['api_user'] = obj.api_user
		param['api_key'] = obj.api_key
		try:
			server = SOAPpy.SOAPProxy(url) 
			session = server.login(param)
		except Exception,e:
			text = "OpenCart Connection in connecting error :"+str(e)
		if session:
			text = 'Test Connection with opencart is successful, now you can proceed with synchronization.'
			status = "Congratulation, It's Successfully Connected with OpenCart Api."
		self.write(cr, uid, ids[0],{'status':status})
		partial = self.pool.get('wizard.message').create(cr, uid, {'text':text})
		return { 'name':_("Information"),
			'view_mode': 'form',
			'view_id': False,
			'view_type': 'form',
			'res_model': 'wizard.message',
			'res_id': partial,
			'type': 'ir.actions.act_window',
			'nodestroy': True,
			'target': 'new',
			'domain': '[]',
		}

	def _create_connection(self, cr, uid, context=None):
		config_id = self.search(cr,uid,[('active','=',True)])
		if len(config_id)>1:
			raise osv.except_osv(_('Error'), _("Sorry, only one Active Configuration setting is allowed."))
		if not config_id:
			raise osv.except_osv(_('Error'), _("Please create the configuration part for OpenCart connection!!!"))
		else:
			param={}		
			session=''
			obj = self.browse(cr,uid,config_id[0])
			url = obj.api_url+'/api/server.php'
			param['api_user'] = obj.api_user
			param['api_key'] = obj.api_key
			try:
				server = SOAPpy.SOAPProxy(url) 
				session = server.login(param)
			except Exception,e:
				raise osv.except_osv(_('Error'), _("OpenCart Connection " + netsvc.LOG_ERROR +  " in connecting: %s") % e)
			if session:
				return [url,session]
			else:
				return False



opencart_configuration()

############ .............Synchronization.............###########

class opencart_sync(osv.osv):
	_name = 'opencart.sync'
	def open_configuration(self, cr, uid, ids, context=None):
		view_id = False
		setting_ids = self.pool.get('opencart.configuration').search(cr, uid, [('active','=',True)])
		if  setting_ids:
			view_id = setting_ids[0]
		return {
			'type': 'ir.actions.act_window',
			'name': 'Configure OpenCart Api',
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'opencart.configuration',
			'res_id': view_id,
			'target': 'current',
			'domain': '[]',
		}


		########## update specific category ##########
	def _update_specific_category(self, cr, uid, id, url, session, context=None):
		get_category_data={}
		cat=''
		cat_pool = self.pool.get('product.category')
		cat_obj = cat_pool.browse(cr,uid,id)
		cat_id = cat_obj.id
		oc_cat_id = cat_obj.opencart_id
		parent_id = cat_obj.parent_id.id or False
		get_category_data['category_id'] = oc_cat_id
		get_category_data['name'] = cat_obj.name
		if parent_id:
			obj_cat = cat_pool.browse(cr,uid,parent_id)
			oc_parent_id = obj_cat.opencart_id or False
			if oc_parent_id:
				get_category_data['parent_id'] = oc_parent_id
			else :
				oc_cat_id = self.sync_categories(cr, uid, url, session, parent_id)
				get_category_data['parent_id'] = oc_cat_id
		else:
			get_category_data['parent_id'] = 0
		try:
			server = SOAPpy.SOAPProxy(url) 
			cat = server.UpdateCategory(session, get_category_data)	
			self.pool.get('product.category').write(cr, uid, id, {'need_sync':'no'})
		except Exception, e:
			return [0, str(e)]
		return  [1, cat]

		########## update category ##########

	def update_category(self,cr,uid,ids,context=None):
		text = text1 = ''
		up_error_ids = []
		success_ids =[]
		connection = self.pool.get('opencart.configuration')._create_connection(cr,uid)
		if connection:
			url = connection[0]
			session = connection[1]
			server = SOAPpy.SOAPProxy(url) 
			cat_ids = self.pool.get('product.category').search(cr,uid,[('need_sync','=','yes')])
			if not cat_ids:
				raise osv.except_osv(_('Information'), _("No category(s) has been found to be Update on OpenCart!!!"))
			if cat_ids:
				for i in cat_ids:
					cat_update=self._update_specific_category(cr, uid, i, url, session)
					if cat_update[0] != 0:
						success_ids.append(i)
					else:
						up_error_ids.append(cat_update[1])
				if success_ids:
					text = 'List of %s Category ids has been sucessfully updated to OpenCart. \n'%success_ids					
				if up_error_ids:
					text1 = 'The Listed Category ids %s does not updated on OpenCart.'%up_error_ids					
				partial = self.pool.get('wizard.message').create(cr, uid, {'text':text+text1})
				return { 'name':_("Information"),
						 'view_mode': 'form',
						 'view_id': False,
						 'view_type': 'form',
						'res_model': 'wizard.message',
						 'res_id': partial,
						 'type': 'ir.actions.act_window',
						 'nodestroy': True,
						 'target': 'new',
						 'domain': '[]',
					 }

	####### create category ############
	def create_category(self,cr,uid,url,session,catg_id,parent_id,catgname):
		cat = parent_id
		opencart_cat_id = 0
		catgdetail = dict({
			'name':catgname,
			'parent_id':parent_id,
			'erp_category_id':catg_id
		})    
		if catg_id > 0:
			try:
				server = SOAPpy.SOAPProxy(url) 
				opencart_cat_id= server.InsertCategory(session, catgdetail)				
			except Exception, e:				
				return str(e)
		else:
			return False
		# return opencart_cat_id
		if opencart_cat_id:
			self.pool.get('product.category').write(cr, uid, catg_id,{'opencart_id':str(opencart_cat_id)})
			return  opencart_cat_id

	####### recursive category sync #########
	def sync_categories(self, cr, uid,url, session, cat_id):
		check=self.pool.get('product.category').search(cr, uid, [('id','=',cat_id),('opencart_id','=',False)])
		if check:
			obj_catg=self.pool.get('product.category').browse(cr,uid,cat_id)
			name=obj_catg.name
			if obj_catg.parent_id.id:
				p_cat_id=self.sync_categories(cr, uid, url, session, obj_catg.parent_id.id)
			else:
				p_cat_id=self.create_category(cr, uid, url,session, cat_id, 0, name)
				return p_cat_id
			category_id=self.create_category(cr, uid, url, session, cat_id, p_cat_id, name)
			return category_id
		else:
			cart_id = self.pool.get('product.category').browse(cr, uid, cat_id).opencart_id
			return cart_id

	#############################################
	##    		Export All Categories  	   	   ##
	#############################################
	
	def export_category(self,cr,uid,ids,context=None):
		length=[]
		connection = self.pool.get('opencart.configuration')._create_connection(cr,uid)			
		if connection[1]:
			url = connection[0]
			session = connection[1]
			map_id=self.pool.get('product.category').search(cr,uid,[('opencart_id','=',False)])
			if map_id:
				for m in map_id:
					cat_id=self.sync_categories(cr, uid, url, session, m)
					length.append(m)
			if len(length)>0:
				text="%s openerp category ids has been Exported to OpenCart."%(length)
			else:
				raise osv.except_osv(_('Information'), _("No new Category(s) found."))
			partial = self.pool.get('wizard.message').create(cr, uid, {'text':text})
			return { 'name':_("Information"),
					 'view_mode': 'form',
					 'view_id': False,
					 'view_type': 'form',
					'res_model': 'wizard.message',
					 'res_id': partial,
					 'type': 'ir.actions.act_window',
					 'nodestroy': True,
					 'target': 'new',
					 'domain': '[]',								 
					}	

	#############################################
	##    export bulk/selected category  	   ##
	#############################################
	
	def export_bulk_category(self, cr, uid, context=None):
		text =  ''
		text1 = text2= ''
		fail_ids= []
		error_ids=[]
		up_error_ids =[]
		success_up_ids=[]
		success_exp_ids= []
		get_product_data = {}
		bulk_ids = context.get('active_ids')	
		map_obj = self.pool.get('product.category')
		connection = self.pool.get('opencart.configuration')._create_connection(cr,uid)
		if connection:
			url = connection[0]
			session = connection[1]			
			for l in bulk_ids:
				search = map_obj.search(cr,uid,[('id','=',l),('opencart_id','=',False)])
				if search:
					cat_id = self.sync_categories(cr, uid, url, session, l)
					if cat_id:
						success_exp_ids.append(l)
				else :
					map_id = self.pool.get('product.category').browse(cr,uid,l)
					if map_id.need_sync == 'yes':
						cat_update=self._update_specific_category(cr, uid,  l, url, session)
						if cat_update[0] != 0:
							success_up_ids.append(l)
						else:
							up_error_ids.append(cat_update[1])
					else:
						fail_ids.append(l)
			if success_exp_ids:
				text = "\nThe listed category ids %s has been created on opencart."%(success_exp_ids)
			if fail_ids:
				text += "\nSelected category ids %s are already Synchronized on opencart."%(fail_ids)
			if success_up_ids:
				text1 = '\nThe listed category ids %s has been successfully updated to opencart. \n'%success_up_ids
			if up_error_ids:
				text2 = '\nThe Listed Product ids %s does not updated on opencart.'%up_error_ids
			partial = self.pool.get('wizard.message').create(cr, uid, {'text':text+text1+text2})
			return { 'name':_("Information"),
					 'view_mode': 'form',
					 'view_id': False,
					 'view_type': 'form',
					'res_model': 'wizard.message',
					 'res_id': partial,
					 'type': 'ir.actions.act_window',
					 'nodestroy': True,
					 'target': 'new',
					 'domain': '[]',
					}	
		
	#############################################
	##    		update all products		       ##
	#############################################
	def update_products(self,cr,uid,ids,context=None):
		text = text1 = ''
		up_error_ids = []
		success_ids =[]
		connection = self.pool.get('opencart.configuration')._create_connection(cr,uid)
		if connection:
			url = connection[0]
			session = connection[1]
			pro_ids = self.pool.get('product.product').search(cr,uid,[('need_sync','=','yes')])
			if not pro_ids:
				raise osv.except_osv(_('Information'), _("No product(s) has been found to be Update on opencart!!!"))
			else:
				for i in pro_ids:
					pro_update=self._update_specific_product(cr, uid, i, url, session)
					if pro_update[0] != 0:
						success_ids.append(i)
					else:
						up_error_ids.append(pro_update[1])
				if success_ids:
					text = 'The Listed Product ids %s has been sucessfully updated to opencart. \n'%success_ids
				if up_error_ids:
					text1 = 'The Listed Product ids %s does not updated on opencart.'%up_error_ids
				text2 = text+text1
				if not text2:
					raise osv.except_osv(_('Information'), _("No product(s) has been found to be Update on OpenCart!!!"))
				partial = self.pool.get('wizard.message').create(cr, uid, {'text':text2})
				return { 'name':_("Information"),
						 'view_mode': 'form',
						 'view_id': False,
						 'view_type': 'form',
						'res_model': 'wizard.message',
						 'res_id': partial,
						 'type': 'ir.actions.act_window',
						 'nodestroy': True,
						 'target': 'new',
						 'domain': '[]',
					 }

	#############################################
	##    		update specific product	       ##
	#############################################
	def _update_specific_product(self, cr, uid, id, url, session, context=None):
		get_product_data = {}
		obj_pro = self.pool.get('product.product').browse(cr,uid,id)
		pro_id = obj_pro.id
		oc_pro_id = obj_pro.opencart_id
		if pro_id and oc_pro_id:
			pro=0
			oc_categ_id=0
			prod_catg=[]
			for j in obj_pro.categ_ids:
				oc_categ_id = self.sync_categories(cr, uid, url, session, j.id)
				prod_catg.append(oc_categ_id)
			if obj_pro.categ_id.id:
				oc_categ_id = self.sync_categories(cr, uid, url, session, obj_pro.categ_id.id)
				prod_catg.append(oc_categ_id)
			get_product_data['product_id'] = oc_pro_id
			get_product_data['name'] = obj_pro.name
			get_product_data['price'] = obj_pro.list_price or 0.00
			get_product_data['weight'] = obj_pro.weight_net or 0.00
			get_product_data['quantity'] = obj_pro.qty_available or 0.00
			get_product_data['product_category'] = prod_catg
			try:
				server = SOAPpy.SOAPProxy(url) 
				pro= server.UpdateProduct(session, get_product_data)	
				self.pool.get('product.product').write(cr, uid, id, {'need_sync':'no'})
			except Exception, e:
				return [0, str(pro_id)+str(e)]
			return  [1, pro]


	#############################################
	##    		single products	create 	       ##
	#############################################
	def prodcreate(self, cr, uid, url, session, pro_id, put_product_data):
		pro=0
		try:
			server = SOAPpy.SOAPProxy(url) 
			pro = server.InsertProduct(session, put_product_data)

		except Exception, e:
			return [0, str(pro_id)+str(e)]
		if pro:
			self.pool.get('product.product').write(cr, uid, pro_id,{'opencart_id':str(pro)})
			return  [1, pro]
	#############################################
	##    		Specific product sync	       ##
	#############################################
	def _export_specific_product(self, cr, uid, id, url, session,context=None):
		"""
		@param code: product Id.
		@param context: A standard dictionary
		@return: list
		"""
		oc_categ_id = 0
		get_product_data = {}
		if id:
			obj_pro = self.pool.get('product.product').browse(cr,uid,id)
			oc_categ_id=0
			prod_catg=[]
			for j in obj_pro.categ_ids:
				oc_categ_id = self.sync_categories(cr, uid, url, session, j.id)
				prod_catg.append(oc_categ_id)
			if obj_pro.categ_id.id:
				oc_categ_id = self.sync_categories(cr, uid, url, session, obj_pro.categ_id.id)
				prod_catg.append(oc_categ_id)
			get_product_data['sku'] = obj_pro.default_code or 'Ref %s'%id
			get_product_data['name'] = obj_pro.name
			get_product_data['description'] = obj_pro.description or ' '
			get_product_data['price'] = obj_pro.list_price or 0.00
			get_product_data['quantity'] = obj_pro.qty_available or 0.00
			get_product_data['weight'] = obj_pro.weight_net or 0.00
			get_product_data['erp_product_id']=obj_pro.id
			get_product_data['product_category'] = prod_catg
			pro = self.prodcreate(cr,uid,url,session,id,get_product_data)
			return pro

	#############################################
	##    		export all products		       ##
	#############################################
	
	def export_products(self,cr,uid,ids,context=None):
		error_ids = []
		success_ids = []
		text = text1 = ''
		connection = self.pool.get('opencart.configuration')._create_connection(cr,uid)	
		if connection:
			url = connection[0]
			session = connection[1]
			server = SOAPpy.SOAPProxy(url) 
			map_id=self.pool.get('product.product').search(cr,uid,[('active','=',True),('opencart_id','=',False)])
			if map_id:
				for m in map_id:
					pro = self._export_specific_product(cr, uid, m, url, session)
				if pro[0] != 0:
					success_ids.append(m)
				else:
					error_ids.append(pro[1])
			else:
				raise osv.except_osv(_('Information'), _("No new product(s) found."))
			if success_ids:
				text = 'The Listed Product ids %s has been sucessfully Exported to OpenCart. \n'%success_ids
			if error_ids:
				text1 = 'The Listed Product ids %s Reference(SKU) has been already exists on opencart.'%error_ids
			partial = self.pool.get('wizard.message').create(cr, uid, {'text':text+text1})
			return { 'name':_("Information"),
					 'view_mode': 'form',
					 'view_id': False,
					 'view_type': 'form',
					'res_model': 'wizard.message',
					 'res_id': partial,
					 'type': 'ir.actions.act_window',
					 'nodestroy': True,
					 'target': 'new',
					 'domain': '[]',
				 }	
	def export_bulk_product(self, cr, uid, context=None):
		text =  ''
		text1 = text2= ''
		fail_ids= []
		error_ids=[]
		up_error_ids =[]
		success_up_ids=[]
		success_exp_ids= []
		bulk_ids = context.get('active_ids')	
		map_obj = self.pool.get('product.product')
		connection = self.pool.get('opencart.configuration')._create_connection(cr,uid)
		if connection:
			url = connection[0]
			session = connection[1]
			for l in bulk_ids:
				search = map_obj.search(cr,uid,[('id','=',l),('opencart_id','=',False)])
				if search:
					
					pro = self._export_specific_product(cr, uid, search[0], url, session)
					if pro[0] != 0:
						success_exp_ids.append(l)
					else:
						error_ids.append(pro[1])
				else :

					map_id = self.pool.get('product.product').browse(cr,uid,l)
					if map_id.need_sync == 'yes':
						pro_update=self._update_specific_product(cr, uid,  l, url, session)
						if pro_update[0] != 0:
							success_up_ids.append(l)
						else:
							up_error_ids.append(pro_update[1])
					else:
						fail_ids.append(l)
			if success_exp_ids:
				text = "\nThe Listed Product ids %s has been created on opencart."%(success_exp_ids)
			if fail_ids:
				text += "\nSelected product ids %s are already synchronized on opnecart."%(fail_ids)
			if error_ids:
				text += '\nThe Listed Product ids %s Reference(SKU) has been already exists on opencart.'%error_ids
			if success_up_ids:
				text1 = '\nThe Listed Product ids %s has been successfully updated to opencart. \n'%success_up_ids
			if up_error_ids:
				text2 = '\nThe Listed Product ids %s does not updated on opencart.'%up_error_ids
			partial = self.pool.get('wizard.message').create(cr, uid, {'text':text+text1+text2})
			return { 'name':_("Information"),
					 'view_mode': 'form',
					 'view_id': False,
					 'view_type': 'form',
					'res_model': 'wizard.message',
					 'res_id': partial,
					 'type': 'ir.actions.act_window',
					 'nodestroy': True,
					 'target': 'new',
					 'domain': '[]',
				}
opencart_sync()

	  

############## Overide Core classes for maintaining OpenCart Information #################
class product_product(osv.osv):	
	_inherit= 'product.product'	
	
	def create(self, cr, uid, vals, context=None):
		if context is None:
			context = {}
		if context.has_key('opencart'):
			if vals.has_key('name'):
				vals['name'] = _unescape(vals['name'])
			if vals.has_key('description'):
				vals['description'] = _unescape(vals['description'])
			if vals.has_key('description_sale'):
				vals['description_sale'] = _unescape(vals['description_sale'])
			if vals.has_key('default_code'):
				vals['default_code'] = _unescape(vals['default_code'])			
		return super(product_product, self).create(cr, uid, vals, context=context)
	
	def write(self,cr,uid,ids,vals,context=None):
		if context is None:
			context = {}
		if context.has_key('opencart'):
			if vals.has_key('name'):
				vals['name'] = _unescape(vals['name'])
			if vals.has_key('description'):
				vals['description'] = _unescape(vals['description'])
			if vals.has_key('description_sale'):
				vals['description_sale'] = _unescape(vals['description_sale'])
			if vals.has_key('default_code'):
				vals['default_code'] = _unescape(vals['default_code'])
		else:
			if type(ids) == list:
				for mid in ids:
					map_id =self.browse(cr,uid,mid).opencart_id
					if map_id:	
						vals['need_sync'] = 'yes'
		return super(product_product,self).write(cr,uid,ids,vals,context=context)
	
	_columns = {
		'opencart_id':fields.char('OpenCart Id'),
		'need_sync': fields.selection((('yes','Yes'),('no','No')),'Update Required'),
		'categ_ids': fields.many2many('product.category','product_categ_rel','product_id','categ_id','Product Categories')
	}
	_defaults={
				'need_sync':'no',
				
	 }
product_product()

class product_category(osv.osv):	
	_inherit= 'product.category'
	
	
	def create(self, cr, uid, vals, context=None):
		if context is None:
			context = {}
		if context.has_key('opencart'):
			if vals.has_key('name'):
				vals['name'] = _unescape(vals['name'])
		return super(product_category, self).create(cr, uid, vals, context=context)
		
	def write(self,cr,uid,ids,vals,context=None):
		if context is None:
			context = {}
		if context.has_key('opencart'):
			if vals.has_key('name'):
				vals['name'] = _unescape(vals['name'])
		else:			
			if type(ids) == list:
				for mid in ids:
					map_id =self.browse(cr,uid,mid).opencart_id
					if map_id:	
						vals['need_sync']='yes'
		return super(product_category,self).write(cr,uid,ids,vals,context=context)
	
	_columns = {
		'opencart_id':fields.char('OpenCart Id'),
		'need_sync': fields.selection((('yes','Yes'),('no','No')),'Update Required'),
	}
	_defaults={
				'need_sync':'no',
				
	 }
product_category()

class res_partner(osv.osv):
	_inherit= 'res.partner'
	def create(self, cr, uid, vals, context=None):
		if context is None:
			context = {}
		if context.has_key('opencart'):
			if vals.has_key('name'):
				vals['name'] = _unescape(vals['name'])
			if vals.has_key('street'):
				vals['street'] = _unescape(vals['street'])
			if vals.has_key('street2'):
				vals['street2'] = _unescape(vals['street2'])
			if vals.has_key('city'):
				vals['city'] = _unescape(vals['city'])
			if vals.has_key('email'):
				vals['email'] = _unescape(vals['email'])
		return super(res_partner, self).create(cr, uid, vals, context=context)
	
	def write(self,cr,uid,ids,vals,context=None):
		if context is None:
			context = {}
		if context.has_key('opencart'):
			if vals.has_key('name'):
				vals['name'] = _unescape(vals['name'])
			if vals.has_key('street'):
				vals['street'] = _unescape(vals['street'])
			if vals.has_key('street2'):
				vals['street2'] = _unescape(vals['street2'])
			if vals.has_key('city'):
				vals['city'] = _unescape(vals['city'])
			if vals.has_key('email'):
				vals['email'] = _unescape(vals['email'])
		return super(res_partner,self).write(cr,uid,ids,vals,context=context)
	
	_columns = {
		'opencart_customer_id':fields.integer('OpenCart Customer Id'),
		'opencart_address_id':fields.integer('OpenCart Address Id'),
		'opencart_type': fields.selection((('customer','Customer'),('address','Address')),'Update Required'),		
	}
	_defaults={
				'opencart_type':'customer',
	 }
res_partner()

class delivery_carrier(osv.osv):
	_inherit= 'delivery.carrier'
	
	def create(self, cr, uid, vals, context=None):
		if context is None:
			context = {}
		if context.has_key('opencart'):
			if vals.has_key('name'):
				vals['name'] = _unescape(vals['name'])
		return super(delivery_carrier, self).create(cr, uid, vals, context=context)	
delivery_carrier()

class sale_order(osv.osv):	
	_inherit= 'sale.order'
	_columns = {
		'opencart_id':fields.char('OpenCart Order Id'),
	}

	def manual_opencart_order_cancel(self, cr, uid, ids, context=None):
		if context is None:
			context = {}
		text = ''
		status = 'no'
		session = False
		oc_cancel = False
		param={}		
		connection = self.pool.get('opencart.configuration').search(cr, uid,[('active','=',True),('order_status','=',True)])
		if connection:
			config_obj =  self.pool.get('opencart.configuration').browse(cr, uid, connection[0])				
			url = config_obj.api_url+'/api/server.php'
			param['api_user'] = config_obj.api_user
			param['api_key'] = config_obj.api_key
			try:
				server = SOAPpy.SOAPProxy(url) 
				session = server.login(param) 
			except:
				pass
			if ids:
				map_id = self.pool.get('sale.order').search(cr, uid, [('id','in',ids),('opencart_id','!=',False)])
				if map_id:
					map_obj = self.pool.get('sale.order').browse(cr, uid, map_id[0])
					oc_order_id = map_obj.opencart_id
					data={}
					data['order_id'] =  oc_order_id	
					data['order_status_id'] =  'cancel'
					try:							 
						oc_cancel = server.UpdateOrderStaus(session, data)
					except Exception, e:
						return str(e)
		cr.commit()		
		return oc_cancel
	
	def write(self,cr,uid,ids,vals,context=None):
		super(sale_order,self).write(cr,uid,ids,vals,context=context)			
		if context is None:
			context = {}
		if isinstance(ids, (int, long)):
			ids = [ids]
		######## manual_opencart_order_cancel method is used to update an cancelled status on opencart end #########
		if vals.has_key('state'):
			if vals['state'] == 'cancel':
				oc_cancel = self.manual_opencart_order_cancel(cr, uid, ids, context)	 
		return True

sale_order()

class account_invoice(osv.osv):
	_name = 'account.invoice'
	_inherit='account.invoice'

	def manual_opencart_invoice(self, cr, uid, ids, context=None):		
		text = ''
		session = 0
		oc_invoice = False
		param={}
		connection = self.pool.get('opencart.configuration').search(cr, uid,[('active','=',True),('order_status','=',True)])
		if connection:
			config_obj =  self.pool.get('opencart.configuration').browse(cr, uid, connection[0])				
			url = config_obj.api_url+'/api/server.php'
			param['api_user'] = config_obj.api_user
			param['api_key'] = config_obj.api_key
			try:
				server = SOAPpy.SOAPProxy(url) 
				session = server.login(param) 
			except:
				pass
			if session:
				map_id = self.pool.get('sale.order').search(cr,uid,[('id','in',ids),('opencart_id','!=',False)])
				if map_id:
					map_obj = self.pool.get('sale.order').browse(cr,uid,map_id[0])
					oc_order_id = map_obj.opencart_id		
					data={}
					data['order_id'] =  oc_order_id	
					data['order_status_id'] = 'paid'	
					try:
						server = SOAPpy.SOAPProxy(url) 
						oc_invoice = server.UpdateOrderStaus(session, data)	
					except Exception,e:
						return str(e)
		cr.commit()	
		return oc_invoice

	def write(self, cr, uid, ids,vals, context=None):
		if context is None:
		    context = {}
		if isinstance(ids, (int, long)):
			ids = [ids]
		######## manual_opencart_invoice method is used to update an invoice status on opencart end #########
		for id in ids:
			if vals.has_key('state'):
				if vals['state'] == 'paid':
					invoice_origin = self.browse(cr, uid, id).origin
					sale_origin_id = self.pool.get('sale.order').search(cr,uid,[('name','=',invoice_origin)])
					if sale_origin_id:
						oc_invoice = self.manual_opencart_invoice(cr, uid, sale_origin_id, context)						
		return super(account_invoice,self).write(cr,uid,ids,vals,context=context)
		###################################################
account_invoice()

class stock_picking(osv.osv):
	_inherit = 'stock.picking'

	def manual_opencart_shipment(self, cr, uid, ids, connection, context=None):
		if context is None:
			context = {}
		text = ''
		status = 'no'
		session = False
		oc_shipment = False
		param={}			
		if ids:
			map_id = self.pool.get('sale.order').search(cr, uid, [('id','in',ids),('opencart_id','!=',False)])
			if map_id:
				config_obj =  self.pool.get('opencart.configuration').browse(cr, uid, connection[0])				
				url = config_obj.api_url+'/api/server.php'
				param['api_user'] = config_obj.api_user
				param['api_key'] = config_obj.api_key
				try:
					server = SOAPpy.SOAPProxy(url) 
					session = server.login(param) 
				except:
					pass
				map_obj = self.pool.get('sale.order').browse(cr, uid, map_id[0])
				oc_order_id = map_obj.opencart_id
				data={}
				data['order_id'] =  oc_order_id	
				data['order_status_id'] =  'delivered'
				try:							 
					oc_shipment = server.UpdateOrderStaus(session, data)
				except Exception, e:
					return str(e)
		cr.commit()		
		return oc_shipment

	@api.cr_uid_ids_context
	def do_transfer(self, cr, uid, picking_ids, context=None):
		return super(stock_picking, self).do_transfer(cr, uid, picking_ids, context)
		connection = self.pool.get('opencart.configuration').search(cr, uid,[('active','=',True),('order_status','=',True)])
		if connection:
			order_name = self.browse(cr, uid,picking_ids[0]).origin
			order_ids = self.pool.get('sale.order').search(cr, uid,[('name','=',order_name)])
			self.manual_opencart_shipment(cr, uid, order_ids, connection, context)
		return True

stock_picking()

